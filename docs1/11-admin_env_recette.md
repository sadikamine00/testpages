

| Application  | Hostname	  | Adresse IP   | Description|
| :---         | :---         | :---         | :---         | 
| Central Governance      | CGRCT      | 172.29.7.196     | Solution centrale pour la gestion & la configuration des serveurs CFT et flux |
| Sentinel      | SENTINELRCT    | 172.29.7.197      | Solution centrale pour la supervision des flux & gestion des événements 
| Gateway Recette      | GATEWAYPRD      | ??.??.??.??      | Gateway pour l'échange des fichiers H2B, multi protocolaire |