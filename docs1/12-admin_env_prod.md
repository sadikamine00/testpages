

| Application  | Hostname	  | Adresse IP   | Description|
| :---         | :---         | :---         | :---         | 
| Central Governance      | CENTGOVPRD       | 172.29.5.100      | Solution centrale pour la gestion & la configuration des serveurs CFT et flux |
| Sentinel      | SENTINELPRD      | 172.29.5.102       | Solution centrale pour la supervision des flux & gestion des événements 
| Gateway Interne      | GATEWAYPRD      | 172.29.7.138      | Gateway Interne pour l'échange des fichiers H2B, multi protocolaire |
| Gateway Externe       | CENTGOVGATPRD       | 172.29.5.101      | Gateway Externe pour toute communication avec les partenaires externe multi protocolaire |
| XSR BDI    | xsrbdi       | 192.168.16.192      | Un Secure relay connecté à la gateway externe dans la DMZ BDI |
| XSR Partenaire    | xsrpart       | 192.168.102.3       | Un Secure relay connecté à la gateway externe dans la DMZ Partenaire ( trusted DMZ ) |
| XSR Publique    | xsrpublique      | 192.168.122.33      | Un Secure relay connecté à la gateway externe dans la DMZ Publique (Untrusted DMZ )|
| Reverse Proxy Egypte  | rpegypte       | 192.168.16.160  | Un haproxy dans la DMZ BDI pour exposer le dashbord de supervision des flux à l'egypte ( exigence sécurité ) | 